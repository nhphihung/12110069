﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogComment.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public String Title { get; set; }
        public String Body { get; set; }
    }
    public class BlogCommentDBContext: DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}