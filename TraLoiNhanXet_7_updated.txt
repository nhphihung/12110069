Em đã làm theo nhận xét số 5 ở bài BaiTap2_GiaoDien_BoSung_3.txt
file PDF iGiaoThong_v5
- Người dùng  cám ơn bài viết bằng cách bấm vào chữ cám ơn
- Trên trang chủ sẽ hiển thị theo mức độ giảm dần theo sự cám ơn (bài nào được cám ơn nhiều sẽ lên trên) vì những tin được cám ơn nhiều là những tin có ích
- Sẽ chia mức độ người dùng theo các cấp
	+ Hạng ruồi: người đăng 1 tin/ngày
	+ Hạng gà: người đăng từ 2-5 tin và được cám ơn 10-20/ngày
	+ Hạng thổ địa: người đăng từ 6-10 tin và được cám ơn >20/ngày
	+ Thứ hạng sẽ được tính cho ngày hôm sau để tránh lên hạng rồi không đăng bài
- Giao diện bạn bè giúp người dùng quản lí bạn bè (có thể hủy bỏ những người không đăng bài hoặc đăng bài không có ích) hoặc thêm bạn mới.
Những bản tin xuất hiện trên trang chủ là những tin do bạn bè đăng. Nếu muốn biết nhiều tin hơn thì vào tìm kiếm.