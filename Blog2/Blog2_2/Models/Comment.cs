﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])[-/](1[0-2]|0[1-9])[-/][0-9]{4}$",ErrorMessage="Nhập theo định dạng dd/mm/yyyy")]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])[-/](1[0-2]|0[1-9])[-/][0-9]{4}$", ErrorMessage = "Nhập theo định dạng dd/mm/yyyy")]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String Author { set; get; }
        
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}