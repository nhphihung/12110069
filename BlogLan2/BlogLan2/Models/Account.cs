﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogLan2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Nhập theo dạng abc@gmail.com")]
        public String Email { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]
        public String FirstName { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự")]
        public String LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}